; (function ($, window) {

    var pluginName = 'galleryPurse';
    var defaults = {
        initialView: 'list',
        events: {}
    };

    //#region common functions
    function handleEmptyAnchor(event) {
        event.preventDefault();
        return false;
    }

    function isMobileView() {
        return window.matchMedia("(max-width:767px)").matches;
    }

    function isTabletView() {
        return window.matchMedia("(min-width:768px) and (max-width:1059px)").matches;
    }

    function isDesktopView() {
        return window.matchMedia("(min-width:1060px)").matches;
    }
    //#endregion

    //#region GalleryPurseView skeleton

    function GalleryPurseView(parent, options) {
        this.parent = parent;
        if (this.defaults !== undefined)
            this.options = $.extend({}, this.defaults, options);
        else
            this.options = options;
        this.$target = $(this.options.target);
    }

    GalleryPurseView.prototype = {
        init: function () {

        },
        hide: function () {
            this.$target.removeClass('visible').addClass('hidden fade');
        },
        show: function () {
            this.$target.removeClass('hidden').addClass('visible');

            if (this.options.index !== null) {
                var self = this;
                setTimeout(function () {
                    self.focusItem(self.options.index);
                }, 1500);
            }
        },
        render: function () {

        },
        renderItem: function (item, index, dataSource) {
            return '';
        },
        renderExtraItem: function (item, index, parent) {
        },
        getItemsToInject: function (index) {
            var extraItems = this.options.dataSource.itemsToInject;

            if (extraItems === undefined || extraItems.length === 0)
                return null;

            return $.grep(extraItems, function (element, elIndex) {
                if (element.position === undefined)
                    return false;

                return element.position === index;
            });
        },
        renderContainer: function () {
            var items = this.options.dataSource.items;
            var $items = $('<div class="items"></div>');
            for (var i = 0; i < items.length; i++) {
                var handled = false;

                if (this.options.events && this.options.events.renderItem) {
                    handled = this.options.events.renderItem({
                        view: this.viewName,
                        parent: $items[0],
                        item: items[i],
                        index: i,
                        dataSource: this.options.dataSource
                    });
                }

                if (!handled) {
                    var itemHtml = this.renderItem(items[i], i, this.options.dataSource);
                    $(itemHtml).appendTo($items);
                }

                var extraItems = this.getItemsToInject(i);

                if (extraItems !== null && extraItems.length > 0) {
                    for (var j = 0; j < extraItems.length; j++) {
                        handled = false;
                        if (this.options.events && this.options.events.renderItem) {
                            handled = this.options.events.renderExtraItem({
                                view: this.viewName,
                                parent: $items[0],
                                item: extraItems[j].element,
                                index: i,
                                dataSource: this.options.dataSource
                            });
                        }
                        if (!handled) {
                            this.renderExtraItem(extraItems[j], i, $items[0]);
                        }
                    }
                }

            }
            this.$target.append($items);
        },
        focusItem: function (index) {
            var offset = this.$target.find('.item[data-index=' + index + ']').offset();
            if (offset)
                $('html, body').animate({ scrollTop: offset.top}, 300);
        },
        //may not need this, but can reattach extra items to DOM
        storeExtraItems: function () {
            var extraItems = this.options.dataSource.itemsToInject;

            if (extraItems === undefined || extraItems.length === 0)
                return;

            for (var i = 0; i < extraItems.length; i++) {
                this.parent.storeElement(extraItems[i].element);
            }
        },
        destroy: function () {
            this.parent = null;
        }
    };
    //#endregion

    //#region GalleryListView methods

    function GalleryListView(parent, options) {
        GalleryPurseView.call(this, parent, options);
        this.viewName = 'list';
        this.intersectionTreshold = 0.1;
        this.currentScrollIndex = 0;
        this.init();
    }

    GalleryListView.prototype = Object.create(GalleryPurseView.prototype);

    GalleryListView.prototype.focusItem= function (index) {
        var self = this;
        var offset = this.$target.find('.item[data-index=' + index + ']').offset();
        if (offset)
            $('html, body').animate({ scrollTop: offset.top}, 300, function(){
                self.initIntersectionObserver();
            });
        else
            this.initIntersectionObserver();
    };

    GalleryListView.prototype.hide = function(){
        GalleryPurseView.prototype.hide.call(this);
        this.intersectionObserver.disconnect();
    };

    GalleryListView.prototype.renderItem = function (item, index, dataSource) {
        var totalItems = dataSource.items.length;

        //wish this is template string
        var titleHtml = item.title ? ('<h3 class="title">' + item.title + '</h3>') : '';
        var imgHtml = '';
        if (item.image) {
            imgHtml = '<div class="img-wrapper">' +
                '<img src="' + item.image + '" />' +
                '</div>';
        }
        var descHtml = item.description ? ('<div class="desc">' + item.description + '</div>') : '';
        var creditHtml = item.credit ? ('<div class="credit">Credit: ' + item.credit + '</div>') : '';

        return '<div class="item" data-index="' + index + '">' +
            titleHtml +
            imgHtml +
            '<div class="links">' +
            '<div class="left">' +
            '<div class="index-info">' +
            '<i class="far fa-clone"></i><span class="index">' + (index + 1) + '</span><span class="separator">/</span><span class="total">' + totalItems + '</span>' +
            '</div>' +
            '</div>' +
            '<div class="right">' +
            '<div class="share"><i class="fas fa-share-alt"></i></div>' +
            '<div class="show-grid"><i class="fas fa-th"></i></div>' +
            '<div class="show-slide"><i class="fas fa-arrows-alt"></i></div>' +
            '</div>' +
            '</div>' +
            '<div class="info">' +
            descHtml +
            creditHtml +
            '</div>' +
            '</div>';
    };


    GalleryListView.prototype.renderExtraItem = function (extraItem, index, parent) {
        var $item = $('<div class="item extra-item"></div>');
        $(extraItem.element).appendTo($item);
        $item.appendTo(parent);
        if (this.options.viewCount > 1 && extraItem.type === 'dfp' && window.googletag && window.googletag.pubads) {
            //TODO: need to optimize with slot parameter
            window.googletag.pubads().refresh();
        }
    };

    GalleryListView.prototype.initIntersectionObserver = function(){
        var self = this;

        if(this.intersectionObserver == null){
            this.intersectionObserver = new window.IntersectionObserver(this.handleIntersection.bind(this), {
                threshold: this.intersectionTreshold
            });     
        }

        this.$target.find('.items .item[data-index]').each(function(index, element){
            self.intersectionObserver.observe(element);
            
        });
    };

    GalleryListView.prototype.handleIntersection = function(entries, observer){
        
        if(entries.length === 0)
            return;

        var self= this;
        
        entries.forEach(function(entry){
            var $el = $(entry.target);
            var currentIndex = parseInt($el.data('index'), 10);
                
            if (entry.intersectionRatio > self.intersectionTreshold && self.currentScrollIndex !== currentIndex) {
                self.currentScrollIndex = currentIndex;
                self.parent.handleIndexChange(self.viewName, currentIndex);
            }
        });
            
      
    };

    GalleryListView.prototype.setHandlers = function () {
        var self = this;

        this.$target.find('.show-grid').click(function () {
            var index = $(this).closest('.item').data('index');
            self.parent.changeView('grid', index);
        });

        this.$target.find('.show-slide').click(function () {
            var index = $(this).closest('.item').data('index');
            self.parent.changeView('fixed-slider', index);
        });

        this.$target.find('.item .img-wrapper img').click(function () {
            var index = $(this).closest('.item').data('index');
            self.parent.changeView('fixed-slider', index);
        });
    };

    GalleryListView.prototype.render = function () {
        GalleryPurseView.prototype.render.call(this);
        this.renderContainer();
        this.setHandlers();
    };

    GalleryListView.prototype.destroy = function () {
        GalleryPurseView.prototype.destroy.call(this);
        this.intersectionObserver = null;
        this.$target.find('.show-grid').off('click');
        this.$target.find('.item .img-wrapper img').off('click');
        this.$target.remove();
        delete this.options;
    };

    //#endregion

    //#region GalleryGridView

    function GalleryGridView(parent, options) {
        this.viewName = 'grid';
        GalleryPurseView.call(this, parent, options);

        this.init();
    }
    GalleryGridView.prototype = Object.create(GalleryPurseView.prototype);

    GalleryGridView.prototype.render = function () {
        GalleryPurseView.prototype.render.call(this);
        this.renderContainer();
        this.setHandlers();
    };

    GalleryGridView.prototype.renderContainer = function () {
        var items = this.options.dataSource.items;
        var topBarHtml = '<div class="top-bar bar">' +
            '<div class="top-bar-inner">' +
            '<div class="right">' +
            '<div class="icon-btn close-grid"><i class="fas fa-times fa-3x"></i></div>' +
            '</div>' +
            '</div>' +
            '</div>';
        this.$target.append(topBarHtml);

        var $items = $('<div class="items"></div>');
        for (var i = 0; i < items.length; i++) {
            var handled = false;
            if (this.options.events && this.options.events.renderItem) {
                handled = this.options.events.renderItem({
                    view: this.viewName,
                    parent: $items[0],
                    item: items[i],
                    index: i,
                    dataSource: this.options.dataSource
                });
            }

            if (!handled) {
                var itemHtml = this.renderItem(items[i], i, this.options.dataSource);
                $(itemHtml).appendTo($items);
            }

        }
        var $itemsWrapper = $('<div class="items-wrapper"></div>');
        $itemsWrapper.append($items);
        this.$target.append($itemsWrapper);

        var bottomBarHtml = '<div class="bottom-bar">' +
            '<div class="bottom-bar-inner">' +
            '<div class="share-text">Share This On</div>' +
            '<div class="addthis_toolbox">' +
            '<a class="addthis_button_facebook share-btn" addthis:title="">' +
            '<i class="fab fa-facebook-f"></i>' +
            '</a>' +
            '<a class="addthis_button_whatsapp share-btn" addthis:title="">' +
            '<i class="fab fa-whatsapp"></i>' +
            '</a>' +
            '<a class="addthis_button_twitter share-btn" addthis:title="">' +
            '<i class="fab fa-twitter"></i>' +
            '</a>' +

            '<a class="addthis_button_pinterest_share share-btn" addthis:title="">' +
            '<i class="fab fa-pinterest"></i>' +
            '</a>' +
            ' <a class="addthis_button_addthis share-btn" addthis:title="">' +
            '<i class="fas fa-plus"></i>' +
            '</a>' +
            '</div>' +
            '</div>' +
            '</div>';
        this.$target.append(bottomBarHtml);

    };

    GalleryGridView.prototype.setHandlers = function () {
        var self = this;

        this.$target.find('.items .item').click(function () {
            var index = $(this).closest('.item').data('index');
            self.parent.changeView('fixed-slider', index);
        });

        this.$target.find('.top-bar .close-grid').click(function () {
            self.parent.returnToPreviousView(self.options.index);
        });

        if (window.addthis !== undefined && window.addthis.toolbox !== undefined) {
            window.addthis.toolbox(this.$target.find('.bottom-bar-inner .addthis_toolbox')[0]);
        }
    }

    GalleryGridView.prototype.renderItem = function (item, index, dataSource) {
        return '<div class="item" data-index="' + index + '">' +
            '<img src="' + item.thumb + '" />' +
            '</div>';
    };

    GalleryGridView.prototype.destroy = function () {
        GalleryPurseView.prototype.destroy.call(this);

        this.$target.find('.items .item').off('click');
        this.$target.find('.top-bar .close-grid').off('click');
        this.$target.remove();
        delete this.options;
    };


    //#endregion

    //#region FixedSliderView

    function FixedSliderView(parent, options) {
        this.viewName = 'fixed-slider';
        GalleryPurseView.call(this, parent, options);
        this.indexMapping = {};
        this.init();
    }

    FixedSliderView.prototype = Object.create(GalleryPurseView.prototype);

    FixedSliderView.prototype.render = function () {
        GalleryPurseView.prototype.render.call(this);
        this.renderContainer();
        this.initSlider();
        this.setHandlers();
    };

    FixedSliderView.prototype.renderContainer = function () {
        var items = this.options.dataSource.items;
        var totalItems = items.length;
        var curIndex = (this.options.index || 0) + 1;
        var topBarHtml = '<div class="top-bar"><div class="top-bar-inner">' +
            '<div class="left">' +
            '<div class="index-info">' +
            '<i class="far fa-clone"></i><span class="index">' + curIndex + '</span><span class="separator">/</span><span class="total">' + totalItems + '</span>' +
            '</div>' +
            '</div>' +
            '<div class="right">' +
            '<div class="show-grid"><i class="fas fa-th"></i></div>' +
            '<div class="close-slide"><i class="fas fa-times"></i></div>' +
            '</div>' +
            '</div></div>';
        this.$target.append(topBarHtml);

        var $sliderWrapper = $('<div class="slider-wrapper"></div>');
        $('<a class="prev" href="javascript:void(0)"><i class="fas fa-chevron-left"></i></a>').appendTo($sliderWrapper);
        $('<a class="next" href="javascript:void(0)"><i class="fas fa-chevron-right"></i></a>').appendTo($sliderWrapper);
        var $items = $('<div class="items slides"></div>');
        var mappingCounter = 0;

        for (var i = 0; i < items.length; i++) {
            var handled = false;

            this.indexMapping[i] = mappingCounter;
            mappingCounter++;

            if (this.options.events && this.options.events.renderItem) {
                handled = this.options.events.renderItem({
                    view: this.viewName,
                    parent: $items[0],
                    item: items[i],
                    index: i,
                    dataSource: this.options.dataSource
                });
            }

            if (!handled) {
                var itemHtml = this.renderItem(items[i], i, this.options.dataSource);
                $(itemHtml).appendTo($items);
            }

            var extraItems = this.getItemsToInject(i);

            if (extraItems !== null && extraItems.length > 0) {
                for (var j = 0; j < extraItems.length; j++) {
                    handled = false;
                    mappingCounter++;

                    if (this.options.events && this.options.events.renderItem) {
                        handled = this.options.events.renderExtraItem({
                            view: this.viewName,
                            parent: $items[0],
                            item: extraItems[j].element,
                            index: i,
                            dataSource: this.options.dataSource
                        });
                    }
                    if (!handled) {
                        this.renderExtraItem(extraItems[j], i, $items[0]);
                    }
                }
            }
        }

        $items.appendTo($sliderWrapper);
        $('<div class="large-prev"></div>').appendTo($sliderWrapper);
        $('<div class="large-next"></div>').appendTo($sliderWrapper);
        

        $('<div class="slider-outer"></div>').append($sliderWrapper).appendTo(this.$target);
        

        var bottomBarHtml = '<div class="bottom-bar">' +
            '<div class="bottom-bar-inner">' +
            '<div class="share-text">Share This On</div>' +
            '<div class="addthis_toolbox">' +
            '<a class="addthis_button_facebook share-btn">' +
            '<i class="fab fa-facebook-f"></i>' +
            '</a>' +
            '<a class="addthis_button_whatsapp share-btn">' +
            '<i class="fab fa-whatsapp"></i>' +
            '</a>' +
            '<a class="addthis_button_twitter share-btn">' +
            '<i class="fab fa-twitter"></i>' +
            '</a>' +
            '<a class="addthis_button_pinterest_share share-btn">' +
            '<i class="fab fa-pinterest"></i>' +
            '</a>' +
            ' <a class="addthis_button_addthis share-btn">' +
            '<i class="fas fa-plus"></i>' +
            '</a>' +
            '</div>' +
            '</div>' +
            '</div>';
        this.$target.append(bottomBarHtml);
    };

    FixedSliderView.prototype.renderItem = function (item, index, dataSource) {
        return '<div class="slide" data-index="' + index + '">' +
            '<div class="figure">' +
            '<div class="caption">' + item.title + '</div>' +
            '<img src="' + item.image + '" />' +
            '<div class="desc"></div>' +
            '</div>' +
            '</div>';
    };

    FixedSliderView.prototype.renderExtraItem = function (extraItem, index, parent) {
        var $item = $('<div class="slide extra-item"></div>');
        var $itemsWrapper = $('<div class="item-wrapper"></div>').appendTo($item);
        var $itemInner = $('<div class="item-inner"></div>').appendTo($itemsWrapper);

        $(extraItem.element).appendTo($itemInner);
        $item.appendTo(parent);
        if (this.options.viewCount > 1 && extraItem.type === 'dfp' && window.googletag && window.googletag.pubads) {
            //TODO: need to optimize with slot parameter
            window.googletag.pubads().refresh();
        }
    };

    FixedSliderView.prototype.initSlider = function () {
        this.$slider = this.$target.find('.slides');
        this.$sliderWrapper = this.$target.find('.slider-wrapper');

        var slickOptions = {
            centerMode: true,
            speed: 700,
            infinite: true,
            autoplaySpeed: 2000,
            slidesToShow: 1,
            slideToScroll: 1,
            cssEase: 'linear',
            prevArrow: $('.prev', this.$sliderWrapper),
            nextArrow: $('.next', this.$sliderWrapper),
            responsive: [{
                breakpoint: 767,
                settings: {
                    centerMode: false,
                    variableWidth: false,
                    slidesToShow: 1
                }
            }]
        };

        if (this.options.index)
            slickOptions.initialSlide = this.indexMapping[this.options.index];

        this.$slider.on('init', this.onSlideInit.bind(this));
        this.$slider.on('beforeChange', this.onBeforeSlideChange.bind(this));
        this.$slider.on('afterChange', this.onAfterSlideChange.bind(this));
        this.$slider.slick(slickOptions);
    };

    FixedSliderView.prototype.updateSlideInfo = function(){
        var index = this.$slider.find('.slick-current').attr('data-index');
        if (index === undefined || index === null) 
            return;
        index = parseInt(index,10);
        var title = this.options.dataSource.items[index].title;
        var url = this.parent.getURL(this.viewName, index);
        this.$target.find('.bottom-bar .addthis_toolbox').attr('addthis:url', url).attr('addthis:title', title);
        
        if (window.addthis !== undefined && window.addthis.toolbox !== undefined) {
            window.addthis.toolbox(this.$target.find('.bottom-bar-inner .addthis_toolbox')[0]);
        }
    };

    FixedSliderView.prototype.onSlideInit = function () {
        if (isDesktopView()) {
            var $current = this.$slider.find('.slick-current');
            $current.prev('.slide').find('.figure img').css({ position: 'absolute', left: '100%', transform: 'translateX(-100%)' });
            $current.find('.figure img').css({ position: 'absolute', left: '50%', transform: 'translateX(-50%)' });
            $current.next('.slide').find('.figure img').css({ position: 'absolute', left: 0, transform: 'translateX(0)' });
        }

        this.updateSlideInfo();
    };

    FixedSliderView.prototype.onBeforeSlideChange = function (event, slick, currentSlide, nextSlide) {
        if (isDesktopView()) {
            var isForward = currentSlide < nextSlide || currentSlide > nextSlide + 1;
            var $next, $newCurrent, $prev;
            if (isForward) {
                $prev = this.$slider.find('.slick-current');
                $newCurrent = $prev.next('.slide');
                $next = $newCurrent.next('.slide');
            } else {
                $next = this.$slider.find('.slick-current');
                $newCurrent = $next.prev('.slide');
                $prev = $newCurrent.prev('.slide');
            }

            $prev.find('.figure img').css({ position: 'absolute', left: '100%', transform: 'translateX(-100%)' });
            $newCurrent.find('.figure img').css({ position: 'absolute', left: '50%', transform: 'translateX(-50%)' });
            $next.find('.figure img').css({ position: 'absolute', left: 0, transform: 'translateX(0)' });
        }
    };

    FixedSliderView.prototype.onAfterSlideChange = function (event, slick, currentSlide) {
        if (isDesktopView()) {
            var $newCurrent = this.$slider.find('.slick-current');
            $newCurrent.prev('.slide').find('.figure img').css({ position: 'absolute', left: '100%', transform: 'translateX(-100%)' });
            $newCurrent.find('.figure img').css({ position: 'absolute', left: '50%', transform: 'translateX(-50%)' });
            $newCurrent.next('.slide').find('.figure img').css({ position: 'absolute', left: 0, transform: 'translateX(0)' });
        }

        //checks if slide is not an injected item
        var index = this.$slider.find('.slick-current').attr('data-index');
        if (index !== undefined) {
            index = parseInt(index, 10);
            this.$target.find('.index-info .index').html(index + 1);
            this.parent.handleIndexChange(this.viewName, index);
            this.updateSlideInfo();
        }
    };

    FixedSliderView.prototype.setHandlers = function () {
        var self = this;
        this.$target.find('.top-bar .show-grid').click(function () {
            var index = self.$slider.find('.slick-current').attr('data-index');
            self.parent.changeView('grid', index);
        });
        this.$target.find('.top-bar .close-slide').click(function () {
            var index = self.$slider.find('.slick-current').attr('data-index');
            self.parent.returnToPreviousView(index);
        });
        this.$target.find('.slider-wrapper .large-prev').click(function(){
            self.$slider.slick('slickPrev');
        });
        this.$target.find('.slider-wrapper .large-next').click(function(){
            self.$slider.slick('slickNext');
        });
        
    };

    FixedSliderView.prototype.destroy = function () {
        GalleryPurseView.prototype.destroy.call(this);

        this.$target.find('.top-bar .show-grid').off('click');
        this.$target.find('.top-bar .close-slide').off('click');
        this.$slider.off('init');
        this.$slider.off('beforeChange');
        this.$slider.off('afterChange');
        this.$target.find('.items-wrapper .large-prev').off('click');
        this.$target.find('.items-wrapper .large-next').off('click');
        this.$slider.slick('unslick');
        this.$target.remove();
        this.$slider = null;

        delete this.options;
    };

    //#endregion

    //#region slider view
    function SliderView(parent, options) {
        this.viewName = 'slider';
        GalleryPurseView.call(this, parent, options);
        this.init();
    }

    SliderView.prototype = Object.create(GalleryPurseView.prototype);

    SliderView.prototype.render = function () {
        GalleryPurseView.prototype.render.call(this);
        this.renderContainer();
        this.initSlider();
        this.setHandlers();
    };

    SliderView.prototype.renderContainer = function () {
        var index = (this.options.index || 0) + 1;
        var items = this.options.dataSource.items;
        var totalItems = items.length;

        var $sliderWrapper = $('<div class="slider-wrapper"></div>');
        $('<a class="prev" href="javascript:void(0);"><i class="fas fa-arrow-left"></i></a>').appendTo($sliderWrapper);
        $('<a class="next" href="javascript:void(0);"><i class="fas fa-arrow-right"></i></a>').appendTo($sliderWrapper);
        var $items = $('<div class="items slides"></div>');
        for (var i = 0; i < items.length; i++) {
            var handled = false;
            if (this.options.events && this.options.events.renderItem) {
                handled = this.options.events.renderItem({
                    view: this.viewName,
                    parent: $items[0],
                    item: items[i],
                    index: i,
                    dataSource: this.options.dataSource
                });
            }

            if (!handled) {
                var itemHtml = this.renderItem(items[i], i, this.options.dataSource);
                $(itemHtml).appendTo($items);
            }
        }

        $items.appendTo($sliderWrapper);
        this.$target.append($sliderWrapper);

        var linksHtml = '<div class="links">' +
            '<div class="left">' +
            '<div class="index-info">' +
            '<i class="far fa-clone"></i><span class="index">' + (index + 1) + '</span><span class="separator">/</span><span class="total">' + totalItems + '</span>' +
            '</div>' +
            '</div>' +
            '<div class="right">' +
            '<div class="share"><i class="fas fa-share-alt"></i></div>' +
            '<div class="show-grid"><i class="fas fa-th"></i></div>' +
            '<div class="show-slide"><i class="fas fa-arrows-alt"></i></div>' +
            '</div>';
        this.$target.append(linksHtml);

        var infoHtml = '<div class="info">' +
            '<div class="title"></div>' +
            '<div class="desc"></div>' +
            '<div class="credit">Credit: <span class="by"></span></div>' +
            '</div>';

        this.$target.append(infoHtml);
    };

    SliderView.prototype.renderItem = function (item, index, dataSource) {
        return '<div class="slide" data-index="' + index + '">' +
            '<img src="' + item.image + '" />' +
            '</div>';
    };

    SliderView.prototype.initSlider = function () {
        this.$sliderWrapper = this.$target.find('.slider-wrapper');
        this.$slider = this.$target.find('.slides');

        var slickOptions = {
            speed: 500,
            infinite: true,
            slidesToShow: 1,
            slideToScroll: 1,
            fade: true,
            cssEase: 'linear',
            prevArrow: $('.prev', this.$sliderWrapper),
            nextArrow: $('.next', this.$sliderWrapper),
        };

        if (this.options.index)
            slickOptions.initialSlide = this.options.index;

        this.$slider.on('init', this.onSlideInit.bind(this));
        this.$slider.on('afterChange', this.onAfterSlideChange.bind(this));
        this.$slider.slick(slickOptions);
    };

    SliderView.prototype.updateSlideInfo = function () {
        var index = parseInt(this.$slider.find('.slick-current').attr('data-index'), 10);
        var title = '', desc = '', credit = '';

        if (index !== undefined && !isNaN(index)) {
            this.$target.find('.links .index-info .index').html(index + 1);
            var item = this.options.dataSource.items[index];
            title = item.title;
            desc = item.description;
            credit = item.credit;
        }

        this.$target.find('.info .title').html(title);
        this.$target.find('.info .desc').html(desc);
        this.$target.find('.info .credit .by').html(credit);
    };

    SliderView.prototype.onSlideInit = function () {
        this.updateSlideInfo();
    };

    SliderView.prototype.onAfterSlideChange = function (event, slick, currentSlide) {
        this.updateSlideInfo();
    };

    SliderView.prototype.setHandlers = function () {
        var self = this;
        this.$target.find('.links .show-grid').click(function () {
            var index = self.$slider.find('.slick-current').attr('data-index');
            self.parent.changeView('grid', index);
        });
        this.$target.find('.links .show-slide').click(function () {
            var index = self.$slider.find('.slick-current').attr('data-index');
            self.parent.changeView('fixed-slider', index);
        });
    };

    SliderView.prototype.destroy = function () {
        GalleryPurseView.prototype.destroy.call(this);

        this.$target.find('.links .show-grid').off('click');
        this.$target.find('.links .show-slide').off('click');

        this.$slider.off('init');
        this.$slider.off('afterChange');
        this.$slider.slick('unslick');
        this.$target.remove();
        this.$slider = null;
    };

    //#endregion

    function UrlHelper(parent, options){
        var defaultOptions = {
            isUrlAffected : false,
            isInternalOnly: false,
            galleryParamName  : null,
            viewParamName  : null,
            slideParamName : null,
        };
        this.parent = parent;
        this.options = $.extend({}, defaultOptions, options);
    } 

    UrlHelper.prototype = {
        //adapted from https://stackoverflow.com/questions/9870512/how-to-obtain-the-query-string-from-the-current-url-with-javascript
        getParamValues:function(value){
            var params = {};
            var arr = value.split('&');

            for (var i = 0; i < arr.length; i++) {
                var param = arr[i].split('=', 2);
                if (param.length !== 2)
                    continue;
                params[param[0]] = decodeURIComponent(param[1].replace(/\+/g, ' '));
            }
            
            return params;
        },

        getInitialValues : function(){
            if(! this.options.isUrlAffected)
                return null;

            var queryString = window.location.search;

            if(queryString === '')
                return null;

            var values = {};
            var params =  this.getParamValues(queryString.substring(1));

            if( this.options.galleryParamName != null && params[this.options.galleryParamName] != null && params[this.options.galleryParamName] != this.parent.id)
                return null;

            if(this.options.viewParamName != null && params[this.options.viewParamName] != null)
                values.viewName = params[this.options.viewParamName];

            if(this.options.slideParamName != null && params[this.options.slideParamName] != null){
                var index = params[this.options.slideParamName];
                index = parseInt(index, 10);
                if(! isNaN(index))
                    values.index = index;
            }
                
            return values;
        },
  
        //params are:
        //- viewName : view name
        //- index
        getUrl : function(options){  
            if(! this.options.isUrlAffected)
                return window.location.href;

            var queryString =window.location.search;
            var params = {};
            var url = [window.location.protocol, '//', window.location.host, window.location.pathname].join('');
            
            if(queryString !== '')
                params =  this.getParamValues(queryString.substring(1));

            if(this.options.viewParamName !== null)
                params[this.options.viewParamName] = options.viewName;

            if(this.options.slideParamName !== null)
                params[this.options.slideParamName] = options.index+1;

            if(this.options.galleryParamName !== null && this.parent.id){
                params[this.options.galleryParamName] = this.parent.id;
            }
            
            var newQueryString = $.param(params);
            if(newQueryString)
                url += '?'+newQueryString;

            return url;
        }
    };

    //#region Plugin methods

    function Plugin(element, options) {
        this.element = element;
        this.options = $.extend({}, defaults, options);
        this._name = pluginName;
        this.view = null;
        this.viewCount = 0;
        this.viewHistory = [];
        this.currentViewName = '';

        if (!this.options.dataSource) {
            this.extractDataSource();
        }

        this.init();
    }


    Plugin.prototype.extractDataSource = function () {
        var $source = null;

        if (this.options.dataSourceElement)
            $source = $(this.options.dataSourceElement);
        else
            $source = $(this.element).find('.data-source');

        if ($source.length === 0)
            return;

        var reClass = /data\-([a-zA-Z0-9-_])+/g;

        function extractDataFromDOM($el, isArrayItem) {
            var $children = $el.children(['class^="data-"']);
            var value = {};

            var isArray = $el.attr('data-type') === 'array';
            var hasHTMLContent = $el.attr('data-type') === 'html';
            var hasHTMLRef = $el.attr('data-type') === 'html-ref';
            var isInt = $el.attr('data-type') === 'int';
            var propName = '';

            if (!isArrayItem) {
                var classes = $el.attr('class');
                if (classes === undefined)
                    return null;

                var propNames = classes.match(reClass);
                if (propNames === null || propNames.length === 0)
                    return null;

                propName = propNames[0].replace('data-', '');

                if (hasHTMLRef) {
                    value[propName] = $el[0];
                    return value;
                }

                if ($children.length == 0 || hasHTMLContent || isInt) {
                    var propValue = $.trim($el.html());

                    if (isInt)
                        propValue = parseInt(propValue, 10);

                    value[propName] = propValue;
                    return value;
                }
            }

            if (isArray)
                value[propName] = [];

            $children.each(function (index, childEl) {
                var $childEl = $(childEl);
                var newValue = extractDataFromDOM($childEl, isArray);

                if (isArray && newValue !== null) {
                    value[propName].push(newValue);
                }
                else {
                    if (newValue !== null)
                        value = $.extend(value, newValue);
                }
            })

            return value;
        }


        if ($source.length === 0)
            return;

        var dataSource = extractDataFromDOM($source);
        if (dataSource !== null)
            this.options.dataSource = dataSource;

        //$source.remove();
    };

    Plugin.prototype.createView = function (viewName, index) {
        if (viewName === 'default')
            viewName = this.options.initialView;

        var $div = $('<div class="' + viewName + '-view hidden"></div>').appendTo(this.purseContainer);
        var viewOptions = {
            dataSource: this.options.dataSource,
            target: $div[0],
            index: index,
            events: this.options.events,
            viewCount: this.viewCount
        };
        switch (viewName) {
            case 'grid':
                return new GalleryGridView(this, viewOptions);
            case 'fixed-slider':
                return new FixedSliderView(this, viewOptions);
            case 'slider':
                return new SliderView(this, viewOptions);
            default:
                return new GalleryListView(this, viewOptions);
        }
    };

    Plugin.prototype.changeView = function (viewName, index, ignoreHistory) {
        if (this.currentViewName === viewName)
            return;

        this.viewCount++;
        var newView = this.createView(viewName, index);
        this.currentViewName = viewName;

        if(! ignoreHistory)
            this.viewHistory.push(viewName);
        
        var oldView = null;
        var self = this;
        if (this.view !== null) {
            oldView = this.view;
        }
        this.view = newView;
        this.view.render();

        if (oldView !== null) {
            oldView.hide();
        }

        setTimeout(function () {
            self.view.show();
        }, 10);

        setTimeout(function () {
            if (oldView !== null) {
                oldView.destroy();
            }
        }, 1500);
    };

    Plugin.prototype.returnToPreviousView = function (index) {
        if (this.viewHistory.length <= 1)
            return;

        this.viewHistory.pop();
        var prevView = this.viewHistory[this.viewHistory.length - 1];
        this.changeView(prevView, index, true);
    };

    Plugin.prototype.handleIndexChange = function(viewName, index){
        var event = $.Event('galleryIndexChange');
        var url = this.getURL(viewName, index);
        var data = { viewName : viewName, index: index };
        if(url != null)
            data.url = url;
        event.galleryInfo = data;
        $(this.element).trigger(event);
    };

    //may not need this code but better attach extra elements to DOM to prevent garbage collection
    Plugin.prototype.storeElement = function (el) {
        var $element = $(this.element);
        var $store = $element.children('.element-store');

        if ($store.length == 0) {
            $store = $('<div class="element-store" style="display:none;"></div>').appendTo($element);
        }
        $store.append(el);
    };

    Plugin.prototype.init = function () {
        var $el = $(this.element);
        var $purseContainer = $el.find('.purse-container');
        if ($purseContainer.length === 0) {
            $purseContainer = $('<div class="purse-container"></div>');
            $purseContainer.appendTo($el);

        }
        this.purseContainer = $purseContainer[0];
        this.urlHelper = new UrlHelper(this, this.options.urlBehavior || {});
        var initialValues = this.urlHelper.getInitialValues();
        
        //TODO: view name validations
        var initialView = this.options.initialView, initialIndex = 0;
        if(initialValues != null){
            if(initialValues.viewName != initialView){
                this.viewHistory.push(initialView);
                initialView = initialValues.viewName;
            }
            if(initialValues.index != null)
                initialIndex = initialValues.index - 1;
        }

        this.changeView(initialView, initialIndex);
    };

    Plugin.prototype.getURL = function(viewName, index){
        return this.urlHelper.getUrl({
            viewName: viewName,
            index: index
        });
    };

    //#endregion

    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName,
                    new Plugin(this, options));
            }
        });
    };

})(jQuery, window);
